//Clasa de baza in care avem functia care trimite mesajul;


package Ro.Orange;

public class Sender {

    //functia send nu intoarce nimic, dar primeste un parametru - mesajul care trebuie trimis - setat in constructorul clasei ThreadedSend
   public void send (String message){

        //punem o pauza de 5 secunde
        try{
            Thread.sleep(5000);

        } catch(InterruptedException e) {
            System.out.println("Thread interrupted");
        }

        //afisam in consola ca mesajul a fost trimis
       System.out.println(message + "\nSent");

    }
}
