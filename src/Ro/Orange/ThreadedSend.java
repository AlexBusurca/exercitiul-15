package Ro.Orange;

public class ThreadedSend extends Thread {

    //cele 3 variabile
    private String msg;
    private Thread t;//n-am inteles de ce am declarat variabila asta
    Sender sender;

    //constructorul primeste mesajul care trebuie trimis si obiectul care face asta
    ThreadedSend(String msg, Sender sender) {
        this.sender = sender;
        this.msg = msg;
    }

    //implementam metoda run din clasa Thread
    public void run() {

        //fiindca folosim un singur obiect sender care este folosit de ambele thread-uri, trebuie sincronizat
        synchronized (sender){

            //afisam in consola ca trimitem mesajul
            System.out.println("Sending " + msg);

            //se trimite propriu-zis mesajul
            sender.send(this.msg);
        }
    }
}
