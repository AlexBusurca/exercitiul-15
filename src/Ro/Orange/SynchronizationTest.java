package Ro.Orange;

public class SynchronizationTest {

    public static void main(String[] args) {

            //declaram un obiect de tip send si il initializam
            Sender snd = new Sender();

            //declaram 2 thread-uri si le initializam cu ajutorul constructorului
            Thread t1 = new ThreadedSend("Hi Terry Williams",snd);
            Thread t2 = new ThreadedSend("We hope you enjoy the stay at our hotel!",snd);

            //pornim thread-urile
            t1.start();
            t2.start();

            //folosim join pe thread doar ca sa exemplificam conceptul, dar in acest exemplu join-ul e inutil
            try{
                t1.join();
                t2.join();
            } catch (InterruptedException e) {}

    }
}
